from api import firestore_db

def state(method,pipeline,**payload):
    
    if len(pipeline) % 2 == 1:
        return f"Error; len(pipeline) is ${len(pipeline)} which is not even"

    if method is "GET":
        if len(pipeline) == 2:
            firestore_db.collection(pipeline[0]).document(pipeline[1]).get()

        if len(pipeline) == 4:
            print(pipeline)
            firestore_db.collection(pipeline[0]).document(pipeline[1]).collection(pipeline[2]).document(pipeline[3]).get()
        
    if method is "SET":
        if len(pipeline) == 2:
            firestore_db.collection(pipeline[0]).document(pipeline[1]).set(payload)

        if len(pipeline) == 4:
            firestore_db.collection(pipeline[0]).document(pipeline[1]).collection(pipeline[2]).document(pipeline[3]).set(payload)

    if method is "UPDATE":
        if len(pipeline) == 2:
            firestore_db.collection(pipeline[0]).document(pipeline[1]).update(payload)

        if len(pipeline) == 4:
            firestore_db.collection(pipeline[0]).document(pipeline[1]).collection(pipeline[2]).document(pipeline[3]).update(payload)

# GLOBAL
# ERRORS
# WORKERS
# ACCOUNT
# TRADES