from multiprocessing import Pool, cpu_count
from time import sleep
from datetime import datetime

from api import firestore_db as db
import requests
import asyncio

def oanda_update_candles():
    r = requests.get("https://europe-west3-at202xcloud.cloudfunctions.net/oanda_update_candles")
def oanda_update_account():
    r = requests.get("https://europe-west3-at202xcloud.cloudfunctions.net/oanda_update_account")

def schema():
    account = db.collection("ACCOUNT")
    candles = db.collection("CANDLES")
    system = {
        "server" : db.collection("SYSTEM").document("server"),
        "database" : db.collection("SYSTEM").document("database"),
        "broker" : db.collection("SYSTEM").document("database"),
    } 

    errors = db.collection("ERRORS")
    trades = db.collection("TRADES")
    workers = db.collection("WORKERS")
    oanda_update_account()
    oanda_update_candles()

def main():
    while True:
        for function in [oanda_update_account, oanda_update_candles]:
            function()
            print("Running...")
            sleep(10)

if __name__ == '__main__':
    schema()
    main()


