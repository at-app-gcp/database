import json
import requests

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin import db 
 
# cred = requests.get("https://europe-west3-at202xcloud.cloudfunctions.net/FIRESTORE_get_JSONcredentials") 
firebase_app = firebase_admin.initialize_app(credentials.Certificate({
  "type": "service_account",
  "project_id": "at202xcloud",
  "private_key_id": "0d0f6e4aace8a2a087a56d9bc52a057638e95991",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCT8aAEiyqpk8Mf\nIlRK0/isT7alPg/R5mUXfl5blyPC7Jfv37HjgDBQP2qXQLOl+Vni7CcFtbGXRvh9\naklqU+oxIXY4UPzN7zUbsms6aFmB6N5wpSJtAbgJo6jsCTKdI3Hi22Efe4wJd1rA\np7CegvCNRoHxcgskEcAAxhPy4acrLuUTUYYrwSV8xUt6VOOatIhAYqlg5B6Dwp57\ndYQnZ2mZJaVRts3Ds1e/9S4Aw5vaUKgOGP8Y6Zml36FmdkUy3F2r05/c8cmEHleY\nnq72sc3LDwnhjBZW+iOR7CzdmsKztdSp3tM4vhKqrJjaIVsa95YYtYWrwbWPuMij\n5QY3b1/3AgMBAAECggEAA3ASmrozz88wTqiuOaUh/mRiBz8vgEHQwcerEhcdOrvj\nYGWUwqPtCbPk4YZx0OlOM9qZaRdxw+UKEoHF/hHcrUuXUTrOxRJKJPkQLiW38IUW\nUprjvpGaGd12mdyjE1e1ERw77jI93pX/pokUWV4oOw5ombTRYXT3PsY/GqWF79Dq\nUuW/ruORrMVUSGqhzqqdcG0z5m9bc3YeTKhIgvg6c0oKAr0V4BoMTuvr6AapaBie\nKV+E1hI5rBhkED6xmB57nX6dKVpaLIbdwUF69WdZXzybUCcxXdWBmTxVUHYkRy5n\ng5HPJteu168Fl89HofhfbzAmvGFN5oyefvY9dXSigQKBgQDJMk3Gli33LJ7mPJfb\nujotdEZ6Z9Mj+EY3cUyAd77CIQptsOHbEFU2bRWVBtwWRo653X940nLWIxiKnTGf\nOeLqf/bgkP2mc7ous66VMKF+DZn3J+VDXrmTQCGNg6CA+DSoP/PcLBe1+LuGinmA\n6sCeGkI7ImChJK260/gy9FYaVwKBgQC8PfGlLbOpFQiYWXO8+/Ax3e7abDXBjTO1\n9FI1Tu6bcmuwAfWuQHlLL6VCBT1i5UOWN9z0Psbl3Junnsd+6TwQEeYWhz6XlChO\nOsxaNqPZVd97fiu0gGE7xTuRqF3yiiegaIABgvcRAaUGQT0u0D8ZsCW3Oko+13mG\nxC8GHgajYQKBgBHTyj5xais/oejGr7IBg0wpxGDuODA0vIwHFv8/HQD4ioMDrE+L\n1/EVo1bsH649nEHt1XCwPQqqW29U1GPGbkvXANoPHtqT4Iho1WxUvcp8jArxVwgs\nKYdZpF90rVEgLRJHcGUu5v7xxjP6qx94LY8Uxo+U0TkWFeq7A4AIVGTLAoGAAdFz\nWQ4U8ygh2h7bEFM6obCsEu4CoujzO+mtNZiak2PKiHusXM+AnGpMZVPZoO/n9DZ7\n6twEXcoAsuDEuoQUMW+K0mLiBuLGdOL6gFCQypOawf2Y/HhU+BfSmHiQ17vf4BvC\nSMF0E3T5nBLbPW2DfoMGCjwE5ZB9OwEAF0YI+GECgYB2KkMNdvhE6WGT7Y4ip09y\nDHJSG2rMf4pN+hwne14Bsm4etrSxGyN1fZ2g0HmwC6lgvbmODcJ0fueGkOl8OZ65\nz/ay8hg4291UPrT7JtVi+BbFXXuunreBOzgWO0PqYiQU1oF6bZUqoYNhsVq4mwuL\ngB0rjVsBeIqs7xDs+NV9eg==\n-----END PRIVATE KEY-----\n",
  "client_email": "owner-sa@at202xcloud.iam.gserviceaccount.com",
  "client_id": "105888736430209448207",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/owner-sa%40at202xcloud.iam.gserviceaccount.com"
}
))
firestore_db = firestore.client()
realtime_db = db