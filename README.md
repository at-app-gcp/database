def run():
    functions = [oanda_update_candles, oanda_update_trades, oanda_update_account]
    pool = Pool(processes=(cpu_count() - 1))
    for fx in functions:
        pool.apply_async(fx)
    pool.close()
    pool.join()